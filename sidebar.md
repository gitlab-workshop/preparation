* Start
    * [Startseite](README.md)
    * [Meine Online Vorbereitung :arrow_upper_right:](https://git.rwth-aachen.de/gitlab-workshop/preparation/-/issues?search=Online+Vorbereitung)
* Online-Vorbereitung
    * [Installation von Git](gitlab-install.md)
    * [Git Basics Versionskontrolle](git-basics.md)
    * Zusammenarbeit mit GitLab
        * [Projekte anlegen und konfigurieren](projects.md)
        * [Dokumentation, Markdown und Wikis](documentation.md)
        * [Projekte und Arbeitspakete planen und verfolgen](issues.md)
        * [Codeschnipsel](snippets.md)
        * [Projekte über HTTPS Clonen](clone.md)
    * GitLab UIs
        * [Installation von VSCodium](install_vscode.md)
    * [Erste Schritte](create-project.md)
