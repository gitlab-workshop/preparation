# Codeschnipsel

Mit GitLab Snippets können Sie Code- und Textteile (Schnipsel oder Snippets) speichern und mit anderen Benutzern teilen.
Gehen Sie dazu auf https://gitlab.com/dashboard/snippets

Es gibt 2 Arten von Snippets, persönliche Snippets und Projekt-Snippets.

## Persönliche Snippets

Persönliche Snippets sind nicht projektbezogen und können unabhängig erstellt werden. Es gibt 3 Sichtbarkeitsebenen: öffentlich, intern und privat.

## Projektsnippets

Projekt-Schnipsel sind immer auf ein bestimmtes Projekt bezogen.

## Snippets Kommentieren

Andere Nutzer können Snippets Kommentieren oder über ein Snippet diskutieren.

## Snippets einbetten

Öffentliche Snippets können nicht nur geteilt, sondern auch in jede Website eingebettet werden. Dies ermöglicht die Wiederverwendung eines GitLab-Ausschnitts an mehreren Stellen und jede Änderung an der Quelle.

Um ein Snippet einzubetten, stellen Sie zunächst sicher, dass:
* Das Projekt ist öffentlich (wenn es sich um einen Projektausschnitt handelt).
* Der Ausschnitt ist öffentlich.
* In Projekt > Einstellungen > Berechtigungen sind die Berechtigungen für Ausschnitte folgende auf Everyone with access eingestellt.

Sobald die oben genannten Bedingungen erfüllt sind, erscheint der Abschnitt "Embed" in Ihrem Snippet.
