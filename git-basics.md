# Git Basics Versionskontrolle

* [Änderungen Machen (3:13)](https://www.youtube.com/watch?v=0ya5jueUlqs)
* [Änderungen Überprüfen (1:15)](https://www.youtube.com/watch?v=XyrnJNrI3cQ)
* [Änderungen Committen (3:02)](https://www.youtube.com/watch?v=mg82bvto4Ug)
* [Versionen Vergleichen (2:50)](https://www.youtube.com/watch?v=-9hyURYmvsY)
* [Änderungen Verwerfen (2:18)](https://www.youtube.com/watch?v=ch-VjQW6tsg)
* [Entwicklungszweige anlegen (6:00)](https://www.youtube.com/watch?v=tHtiehTr59I)
* [Dateien löschen/entfernen (2:17)](https://www.youtube.com/watch?v=QHBTc92WT1E)
* [Entwicklungszweige zusammenführen (6:28)](https://www.youtube.com/watch?v=xyTS2yyOWnA)
asd