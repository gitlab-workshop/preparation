# Einführung in git und FDM mit GitLab

Datum: 24.06.2022, 10:00 - 16:00 Uhr

Format: Online, Flipped Classroom in und mit GitLab, ~1,5h Vorbereitungszeit

Technik Check: 15.06.2022 15:00 Uhr

Herzlich willkommen zur online-Vorbereitung zum Gitlab-Workshop *Einführung in git und FDM mit GitLab*

## Was ist git/Gitlab?

Git ist eine etablierte Software zur Versionsverwaltung von Dateien. Wenn auch voranging zum Code-Management in der Softwareentwicklung genutzt, können in der Praxis nahezu jede Art von Datei per Versionsverwaltung nachverfolgt werden. Gut geeignet ist git für flach strukturierte, textbasierte Datensätze wie csv, xml usw.

Die Weboberfläche GitLab ist eine Open-Source-Software, um eigene git-Repositories selbst zu hosten und zu verwalten. Neben der Hauptaufgabe des Code-Managements werden noch andere Funktionalitäten wie ein einfaches Issue-Tracking-System, Wiki sowie Code-Review-Möglichkeiten abgedeckt. GitLab wird an vielen Hochschulen lokal angeboten oder kann wie [GitLab an der RWTH Aachen](https://git-ce.rwth-aachen.de) in Kooperation genutzt werden.

Um ein gemeinsames Arbeiten zu erleichetern verwenden wir im Workshop die EE-Variante von GitLab: [https://git.rwth-aachen.de](https://git.rwth-aachen.de).

## Online-Vorbereitung

Zur vorbereitung auf den Workshop stellen wir Ihnen Lernmaterialien zur Verfügung, die Sie dazu anleiten, git auf Ihrem Rechner zu installieren, ein Projekt einzurichten und erste einfache Befehle auszuführen. Der online-Kurs gliedert sich in die Installation von git, git Basics zur Versionskontrolle und in eine Einführung in die Zusammenarbeit mit GitLab. Um alles auszuprobieren sollten Sie bis zu einen Vormittag Zeit einplanen. Sollten Sie während Ihrer online-Vorbereitung Fragen haben, können Sie diese als [Issue / Ticket](https://git.rwth-aachen.de/gitlab-workshop/preparation/-/issues) stellen. Wir werden versuchen, diese dann zeitnah zu beantworten.

Am Ende der online-Vorbereitung sollten Sie git auf Ihrem Rechner installiert, ein erstes Projekt angelegt und erste einfache Befehle mit git ausgeführt haben. Sie haben sich auf der [GitLab Web-Oberfläche](https://git.rwth-aachen.de) eingeloggt und sollten in der Lage sein, mit GitLab loslegen zu können.

Im Workshop werden Sie weitere git/GitLab-Funktionalitäten kennenlernen. Gemeinsam werden wir im Workshop Anwendungsbeispiele diskutieren und Übungen zu individuellen Arbeitsabläufe und Best Practices durchführen. Bitte stellen Sie sicher, dass Sie während dem Workshop zugriff auf die git-Software und und die [GitLab Web-Oberfläche](https://git.rwth-aachen.de) haben.

Um Ihren Fortschritt bei der Online-Vorbereitung zu verfolgen können Sie eine Checkliste in Gitlab erstellen. Dafür müssen Sie einen Account bei GitLab.com einrichten:

1. Anmelden bei GitLab.com
1. Neuen Arbeitsbereich für die Vorbereitung erstellen

### Anmelden bei&nbsp;[https://git.rwth-aachen.de](https://git.rwth-aachen.de)

Im Workshop soll gemeinsam mit den anderen Teilnehmern ausprobiert werden wie sich git und GitLab in einer kleinen Gruppe Nutzen lassen. Dafür müssen alle Teilnehmer einen Account auf einem GitLab server haben. Im workshop nutzen wir die EE-Variante von GitLab: [https://git.rwth-aachen.de](https://git.rwth-aachen.de).

#### Erster Kontakt

Falls Sie noch keinen Account auf [https://git.rwth-aachen.de](https://git.rwth-aachen.de) haben müssten Sie sich zunächst registrieren. Öffenen Sie dazu die Webseite https://git.rwth-aachen.de und Nutzen Sie die Option "DFN-AAI Single Sign-On" und loggen Sie sich mit Ihrer RWTH-ID ein.

### Arbeitsbereich für die Vorbereitung

Für die Vorbereitung sollten Sie einen eigenen Arbeitsbereich erstellen. Dort können Sie ihren individuellen Fortschritt bei der Vorbereitung nachvollziehen:

:arrow_right: [Jetzt einen neuen Arbeitsbereich erstellen](https://git.rwth-aachen.de/gitlab-workshop/preparation/-/issues/new?issuable_template=Preparation&issue[title]=Online+Vorbereitung) :arrow_left:

(oder [bestehenden Arbeitsbereich anzeigen](https://git.rwth-aachen.de/gitlab-workshop/preparation/-/issues?search=Online+Vorbereitung))

Klicken Sie auf der verlinkten Seite dafür einfach unten auf die Schaltfläche "Submit Issue" bzw. "Submit ticket".

Auf dem folgenden Bildschirm erhalten Sie eine Checkliste für die Online Vorbereitung.

### Ohne Arbeitsbereich vorbereiten

Falls Sie sich ohne Arbeitsbereich vorbereiten möchten finden Sie auch alle Inhalte hier auf der Seite in der Navigation unter dem Punkt "Online-Vorbereitung".
