<!--

Sie erstellen einen privaten Arbeitsbereich zur Online-Vorbeiretung für den Workshop. 

Klicken Sie dafür einfach unten auf die Schaltfläche "Create issue" bzw. "Issue erstellen".

Auf dem folgenden Bildschirm erhalten Sie eine Checkliste für die Online Vorbereitung.































-->

Die Online-Vorbereitung besteht aus einem Mix aus Videos, Anleitungen und kleinen "Schritt-für-Schritt"-Aufgaben. 

Wenn Sie eine Einheit abgeschlossen haben, können Sie diese mit der Checkbox markieren. So behalten Sie einen Überblick. 

Sie finden Ihre eigene Checkliste für die Online Vorbereitung jederzeit unter der aktuellen URL oder unter diesem [Link](../../issues?search=Online+Vorbereitung).

#### Basics

* [x] Anmelden bei GitLab.com
* [x] [Arbeitsbereich erstellen](../../issues?search=Online+Vorbereitung)

#### Installation von Git

* [ ] [Installation, Mac OSX (2:49)](https://www.youtube.com/watch?v=TvrZw47e7tI)
* [ ] [Installation, Windows (1:53)](https://www.youtube.com/watch?v=5KFn0r2XrtA)
* [ ] [Installationsanleitung, Windows](install_windows.md)
* [ ] [Installationsanleitung VSCodium](install_vscode.md)
* [ ] [Installation, Linux (2:13)](https://www.youtube.com/watch?v=HSOuBmiCdrM)
* [ ] [Konfigurieren (2:46)](https://www.youtube.com/watch?v=PegV5zz5iFU)
* [ ] [Repository Anlegen, Mac OSX (2:51)](https://www.youtube.com/watch?v=Bo-pKqHO2go)
* [ ] [Repository Anlegen, Windows (6:34)](https://www.youtube.com/watch?v=8Qau5_NmF9s)

#### Git Basics Versionskontrolle

* [ ] [Änderungen Machen (3:13)](https://www.youtube.com/watch?v=0ya5jueUlqs)
* [ ] [Änderungen Überprüfen (1:15)](https://www.youtube.com/watch?v=XyrnJNrI3cQ)
* [ ] [Änderungen Committen (3:02)](https://www.youtube.com/watch?v=mg82bvto4Ug)
* [ ] [Versionen Vergleichen (2:50)](https://www.youtube.com/watch?v=-9hyURYmvsY)
* [ ] [Änderungen Verwerfen (2:18)](https://www.youtube.com/watch?v=ch-VjQW6tsg)
* [ ] [Entwicklungszweige anlegen (6:00)](https://www.youtube.com/watch?v=tHtiehTr59I)
* [ ] [Dateien löschen/entfernen (2:17)](https://www.youtube.com/watch?v=QHBTc92WT1E)
* [ ] [Entwicklungszweige zusammenführen (6:28)](https://www.youtube.com/watch?v=xyTS2yyOWnA)

#### Zusammenarbeit mit GitLab

* [ ] [Projekte anlegen und konfigurieren](projects.md)
* [ ] [Dokumentation, Markdown und Wikis](documentation.md)
* [ ] [Projekte und Arbeitspakete planen und verfolgen](issues.md)
* [ ] [Codeschnipsel](snippets.md)
* [ ] [Projekte über HTTPS Clonen](clone.md)

#### Arbeiten mit GitLab Projekten

Nachdem sie die Einheiten oben abgeschlossen haben, sollten Sie folgenden Ablauf ausprobieren. Versuchen Sie so weit zu kommen wie möglich. Falls Sie nicht ganz durchkommen: Keine Angst! Die Übung werden wir im Workshop wieder aufgreifen:

* Loggen Sie sich bei [GitLab.com](https://www.gitlab.com) ein
    * [ ] Erstellen Sie ein neues Projekt mit dem Namen `gitlab-nrw-wokshop`
    * [ ] Initialisieren Sie das Projekt mit einer README Datei
    * [ ] Klicken Sie auf die README Datei um den Ihalt der Datei anzusehen
    * [ ] Klicken Sie auf den Button "Edit"/"Bearbeiten" und machen Sie eine Änderung an der README Datei im Browser
    * [ ] Gehen Sie zurück auf die Projektseite und kopieren sie die URL zum Herunterladen des Projekts unter dem `Clone`-Button. Achten Sie darauf, die HTTPS URL zu kopieren    
* Erstellen Sie eine lokale Kopie um mit dem Repository zu arbeiten
    * [ ] Öffnen Sie die Kommandozeile um mit git auf ihrem Computer zu arbeiten.
    * [ ] Erstellen Sie einen neuen, leeren Ordner. Zum Beispiel mit dem Kommando `mkdir wokshop`
    * [ ] Wechseln Sie in den Ordner mit dem Kommando `cd wokshop`
    * [ ] Laden Sie das Projekt mit dem Kommando `git clone [URL]` herunter
    * [ ] `git` fragt beim Herunterladen nach Nutzername und Passwort - siehe "[Projekte über HTTPS Clonen](clone.md)" für details.
* Machen Sie eine Änderung in der README Datei, diesmal in der lokalen Kopie des Repositories
    * [ ] Committen Sie die Änderung in ihrer lokalen Kopie.
    * [ ] Übertragen sie die Änderung mit dem Kommando `git push origin main` an GitLab
    * [ ] Überprüfen Sie im Browser ob die Änderung in GitLab angekommen ist.

    


/confidential
/assign me
/due 24.06.2022
/title Online Vorbereitung

<!--

















Sie erstellen einen privaten Arbeitsbereich zur Online-Vorbeiretung für den Workshop. 

Klicken Sie dafür einfach unten auf die Schaltfläche "Create Issue" bzw. "Issue erstellen".

Auf dem folgenden Bildschirm erhalten Sie eine Checkliste für die Online Vorbereitung.











-->
